# yt_upload:

## Description:
Projekt zum Automatisierten Upload von YT Videos. Es soll Videos Automatisiert hochladen können. Es soll in einer Datenbank den status zu den Videos Festhalten (Aufgenommen, Im Upload, Hochgeladen), sowie die dazugehörigen informationen speichern. Was ist hochgeladen. Wie heist es, beschreibung...

## Technologien
 - Sprache: Rust
 - Db: Mysql(vorerst)

## Features:
 - Video Upload
 - Db Eintragung von Upload
 - Hochgeladene Videos löschen können
 - Upload kann anhand eines uploadplans erfolgen

## Ablauf (grob)
 1. Video wurde aufgenommen.
 2. Video wird erfasst.
 3. Video wird in db eingetragen
 4. Videos die den Upload zeitpunkt erreicht haben werden hochgeladen
 5. Video wird gelöscht
 6. db wird aktualisiert

## DbModel:

### tbl_video
| Field | Type | Sonstiges |
|-------|------|-----------|
| id_video | int | AI PK |
| original_name | varchar(255) | not null |
| create_date | DateTime | Not Null |
| upload_status | varchar(10) | Not NUll |
| upload_date | DateTime | Not null |
| video_link | varchar 255 | Not Null |
| video_details_id | int | FK Not Null |

### tbl_video_details
| Field | Type | Sonstiges |
|-------|------|-----------|
| id_video_details | int | AI PK |
| title | varchar(100) | Not Null |
| description | varchar(500) | Not Null |
| game_id | int | FK |
| playlist_id | int | FK |
| create_date | DateTime | Not Null |

### tbl_playlist
| Field | Type | Sonstiges |
|-------|------|-----------|
| id_playlist | int | AI PK |
| title | varchar(1000) | Not Null |
| create_date | DateTime | Not Null |

### tbl_game
| Field | Type | Sonstiges |
|-------|------|-----------|
| id_game | int | AI PK |
| title | varchar(255) | NOT NULL |

## Anmerkungen
 - Ja noch sehr sporadisch, weil ich noch im laufe der zeit mehr plane

## Programm Aufbau:

### database_updater:
 - Hält die Daten in der Datenbank Up to date zu den videos
 - Läuft eigenständig im hintergrund, kann aber eigenständig getriggert werden
 - stellt eine brücke zwischen dem uploadsystem informationen und youtube dar
    - beschreibungen und co werden hierrüber aktualisiert

#### Commands
yt_upload update run
 - startet den autoupdater für die daten in der db und bei youtube

yt_upload update run -interval 60
 - startet den autoupdater für die daten inder db und bei youtube
 - gibt einen interval in dem geupdated werden soll an

yt_upload update stop
 - stopped das update programm sobald es kann
 - lässt das programm aktionen sauber beenden und laufende updates zuende machen

yt_upload update stop -force
 - stopped das update pr

### video_uploader:
 - Lädt videos die in der Datenbank eingetragen sind hoch
 - Lädt ausgewählte videos hoch
 - kann auch videos nach datum und reihenfolge oder playlist hochladen 
 - läuft im hintergrund

#### Commands
yt_upload upload run
 - Startet das programm zum upload der videos
 - lädt automatisch scheduled videos hoch
 - aktualisert dabei auch die datenbank

yt_upload upload enqueue -path ... 
 - lädt die sich im ordner befindlichen datein gemäß der einstellungen hoch

yt_upload upload enqueue -file ... 
 - lädt die gewählte datei hoch

yt_upload upload schedule -file ... -date
 - ändert den die zeit wenn ein video hochgeladen wird

### video_index:
 - trägt neue videos in der datenbank ein.
 - wenn gestartet muss der benutzer für alle noch nicht existenten videos einen die details eintragen
 - Informationen können auch über eine gleich benanntes Rust Object File eigetragen werden

#### Commands
yt_upload index run
 - startet die indexierung aller noch nicht hochgeladenen dateien

yt_upload index alter local -path [<...>]
 - startet den änderungsmodus für die lokalen aufnahmen
 - optionaler weise kann der ordner angegeben werden. ohne wird der haupt aufnahme ordner genommen.

yt_upload index alter db 
 - startet den änderungsmodus für einträge in der datenbank

## Upload file description

```rs
struct UploadDetails {
    schedule_for: Option<DateTime<Utc>>,
    state: UploadState,
    details: Details,
    playlist: Vec<String>,
}

enum UploadState {
    Draft,
    Indexed,
    Uploaded,
}

struct Details {
    title: String,
    description: String,
    game: String,
}
```
ergibt
```
UploadDetails(
    schedule_for: None,
    state: Draft,
    details: {
        title: "Starcrafts #1",
        description: "This is a description.",
        game: "StarCraft: Remastered (2017)",
    },
    playlist: [
        "Starcrafts", 
        "Strategy Time"
    ]
)
```

## File system sample aufbau:
 - ROOT
   - 2019_12_16_14_47.mp4
   - 2019_12_16_14_47.ron
   - Starcraft
     - 2019_12_16_14_49.mp4
     - 2019_12_16_14_49.ron

## Wichtige Speichersettings:
 - base_folder: Basisordner von dem aus das programm ausgeht
 - database: Datenbankverbindung