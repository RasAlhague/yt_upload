use yt_upload::Config;

fn main() {
    let config = Config::new();

    yt_upload::run(config);
}
