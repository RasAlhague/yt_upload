#[macro_use]
extern crate mysql;

pub mod models;
pub mod playlist;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Config {
    #[structopt(short, long)]
    pub db_conn_url: String,
}

impl Config {
    pub fn new() -> Config {
        Config::from_args()
    }
}

pub fn run(config: Config) {
    if !test_connection(&config.db_conn_url.clone()) {
        return;
    }
}

pub fn test_connection(db_url: &str) -> bool {
    match mysql::Pool::new(db_url) {
        Ok(_) => return true,
        Err(err) => {
            println!("Couldn't connect to Database: {}", err);

            return false;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_test_connection() {
        let conn_str = "mysql://root:PENIS@localhost:3306/yt_db";

        assert!(test_connection(conn_str));
    }

    #[test]
    fn test_bad_test_connection() {
        let conn_str = "mysql://root:PENIS@localhost:3306/yt_db2";

        assert!(!test_connection(conn_str));
    }
}
