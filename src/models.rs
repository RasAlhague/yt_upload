use mysql::Pool;

pub trait Create {
    type Model;

    fn create(model: Self::Model, pool: Pool) -> Result<Option<Self::Model>, String>;
    fn create_entry(&mut self, pool: Pool) -> Result<(), String>;
}

pub trait Read {
    type Model;

    fn get(id: u32, condition: &str, pool: Pool) -> Result<Option<Self::Model>, String>;
    fn get_all(pool: Pool) -> Result<Option<Vec<Self::Model>>, String>;
    fn get_entry(&mut self, pool: Pool) -> Result<(), String>;
}

pub trait Update {
    type Model;

    fn update(model: Self::Model, pool: Pool) -> Result<Option<Self::Model>, String>;
    fn update_entry(&mut self, pool: Pool) -> Result<(), String>;
}

pub trait Delete {
    type Model;

    fn delete(id: u32, pool: Pool) -> Result<bool, String>;
    fn delete_all(pool: Pool) -> Result<bool, String>;
}

pub fn last_inserted_id(
    tbl_schema_name: &str,
    id_name: &str,
    pool: Pool,
) -> Result<Option<u32>, String> {
    let sql = "SELECT MAX($id) as id FROM $table"
        .replace("$table", tbl_schema_name)
        .replace("$id", id_name);

    let mut rows = match pool.prep_exec(sql, ()) {
        Ok(r) => r,
        Err(err) => {
            println!("{}", err);
            return Err(String::from("Error while retrieving last inserted id!"));
        }
    };

    let row = rows.nth(0);

    let id: u32 = mysql::from_row(row.unwrap().unwrap());

    return Ok(Some(id));
}
