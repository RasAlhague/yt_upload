use crate::models::*;
use mysql::chrono::DateTime;
use mysql::chrono::NaiveDateTime;
use mysql::chrono::Utc;
use mysql::Pool;

pub static TABLE_SCHEMA_NAME: &str = "tbl_playlist";

pub struct Playlist {
    pub id_playlist: u32,
    pub title: String,
    pub create_date: DateTime<Utc>,
}

impl Playlist {
    pub fn empty() -> Playlist {
        Playlist {
            id_playlist: 1,
            title: String::new(),
            create_date: Utc::now(),
        }
    }
}

impl Create for Playlist {
    type Model = Playlist;

    fn create(model: Self::Model, pool: Pool) -> Result<Option<Self::Model>, String> {
        let sql = format!("INSERT INTO {} 
                                (title)
                            VALUES
                                (:title)", TABLE_SCHEMA_NAME);

        for mut stmt in pool.prepare(sql) {
            stmt.execute(params! {
                "title" => model.title.clone(),
            })
            .unwrap();
        }

        let last_id = match last_inserted_id(TABLE_SCHEMA_NAME, "id_playlist", pool.clone()) {
            Ok(o) => match o {
                Some(id) => id,
                None => {
                    return Ok(None);
                }
            },
            Err(err) => {
                println!("{}", err);
                return Err(String::from("Couldn't retrieve last id!"));
            }
        };
        Playlist::get(last_id, "", pool.clone())
    }

    fn create_entry(&mut self, pool: Pool) -> Result<(), String> {
        let self_copy = Playlist {
            id_playlist: 0,
            title: self.title.clone(),
            create_date: Utc::now(),
        };

        let inserted_playlist = match Playlist::create(self_copy, pool.clone()) {
            Ok(playlist) => match playlist {
                Some(p) => p,
                None => {
                    return Err(String::from("Unknown error"));
                }
            },
            Err(err) => {
                println!("{}", err);
                return Err(String::from("Couldn't retrieve last id!"));
            }
        };

        self.title = inserted_playlist.title;
        self.id_playlist = inserted_playlist.id_playlist;
        self.create_date = inserted_playlist.create_date;
        Ok(())
    }
}

impl Read for Playlist {
    type Model = Playlist;

    fn get(id: u32, condition: &str, pool: Pool) -> Result<Option<Self::Model>, String> {
        let mut sql = format!("SELECT id_playlist, title, create_date
                                FROM {}
                                WHERE id_playlist = :id_playlist", TABLE_SCHEMA_NAME);

        sql.push_str(condition);

        let mut rows = match pool.prep_exec(
            sql,
            params! {
                "id_playlist" => id,
            },
        ) {
            Ok(r) => r,
            Err(err) => {
                println!("{}", err);
                return Err(String::from("Error while retrieving last inserted id!"));
            }
        };
        let row = rows.nth(0);

        let (id_playlist, title, create_date): (u32, String, NaiveDateTime) =
            mysql::from_row(row.unwrap().unwrap());

        let date = DateTime::<Utc>::from_utc(create_date, Utc);

        Ok(Some(Playlist {
            id_playlist,
            title,
            create_date: date,
        }))
    }

    fn get_all(pool: Pool) -> Result<Option<Vec<Self::Model>>, String> {
        let sql = format!("SELECT id_playlist, title, create_date 
                            FROM {}", TABLE_SCHEMA_NAME);

        let rows = match pool.prep_exec(sql, ()) {
            Ok(r) => r,
            Err(err) => {
                println!("{}", err);
                return Err(String::from("Error while retrieving last inserted id!"));
            }
        };

        let mut playlists: Vec<Playlist> = Vec::new();

        for row in rows {
            let (id_playlist, title, create_date): (u32, String, NaiveDateTime) =
                mysql::from_row(row.unwrap());

            let date = DateTime::<Utc>::from_utc(create_date, Utc);

            playlists.push(Playlist {
                id_playlist,
                title,
                create_date: date,
            });
        }

        Ok(Some(playlists))
    }

    fn get_entry(&mut self, pool: Pool) -> Result<(), String> {
        let sql = format!("SELECT id_playlist, title, create_date
                            FROM {}
                            WHERE id_playlist = :id_playlist", TABLE_SCHEMA_NAME);

        let mut rows = match pool.prep_exec(
            sql,
            params! {
                "id_playlist" => self.id_playlist,
            },
        ) {
            Ok(r) => r,
            Err(err) => {
                println!("{}", err);
                return Err(String::from("Error while retrieving last inserted id!"));
            }
        };
        let row = rows.nth(0);

        let (id_playlist, title, create_date): (u32, String, NaiveDateTime) =
            mysql::from_row(row.unwrap().unwrap());

        let date = DateTime::<Utc>::from_utc(create_date, Utc);

        let playlist = Playlist {
            id_playlist,
            title,
            create_date: date,
        };

        self.create_date = playlist.create_date;
        self.id_playlist = playlist.id_playlist;
        self.title = playlist.title;

        Ok(())
    }
}

impl Update for Playlist {
    type Model = Playlist;

    fn update(model: Self::Model, pool: Pool) -> Result<Option<Self::Model>, String> {
        let sql = format!("UPDATE {}
                            SET title = :title
                            WHERE id_playlist = :id_playlist", TABLE_SCHEMA_NAME);

        for mut stmt in pool.prepare(sql) {
            stmt.execute(params! {
                "title" => model.title.clone(),
                "id_playlist" => model.id_playlist,
            })
            .unwrap();
        }

        Playlist::get(model.id_playlist, "", pool.clone())
    }

    fn update_entry(&mut self, pool: Pool) -> Result<(), String> {
        let sql = format!("UPDATE {}
                            SET title = :title
                            WHERE id_playlist = :id_playlist", TABLE_SCHEMA_NAME);

        for mut stmt in pool.prepare(sql) {
            stmt.execute(params! {
                "title" => self.title.clone(),
                "id_playlist" => self.id_playlist,
            })
            .unwrap();
        }

        let pl = Playlist::get(self.id_playlist, "", pool.clone()).unwrap().unwrap();

        self.id_playlist = pl.id_playlist;
        self.title = pl.title;
        self.create_date = pl.create_date;

        Ok(())
    }
}

impl Delete for Playlist {
    type Model = Playlist;

    fn delete(id: u32, pool: Pool) -> Result<bool, String> {
        let sql = format!("DELETE FROM {} WHERE id_playlist = :id_playlist", TABLE_SCHEMA_NAME);

        for mut stmt in pool.prepare(sql) {
            stmt.execute(params! {
                "id_playlist" => id,
            })
            .unwrap();
        }

        Ok(true)
    }

    fn delete_all(pool: Pool) -> Result<bool, String> {
        let sql = format!("DELETE FROM {}", TABLE_SCHEMA_NAME);

        for mut stmt in pool.prepare(sql) {
            stmt.execute(())
            .unwrap();
        }

        Ok(true)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create() {
        let conn_str = "mysql://root:PENIS@localhost:3306/yt_db";

        let pool = Pool::new(conn_str).unwrap();

        Playlist::delete_all(pool.clone()).unwrap();

        let mut playlist = Playlist::empty();
        playlist.id_playlist = 0;
        playlist.title = String::from("Test Playlist");

        let inserted_playlist = Playlist::create(playlist, pool)
            .unwrap()
            .unwrap();

        assert_eq!(inserted_playlist.title, String::from("Test Playlist"));
    }

    #[test]
    fn test_create_entry() {
        let conn_str = "mysql://root:PENIS@localhost:3306/yt_db";

        let pool = Pool::new(conn_str).unwrap();

        Playlist::delete_all(pool.clone()).unwrap();

        let mut playlist = Playlist {
            id_playlist: 0,
            title: String::from("Test Playlist"),
            create_date: Utc::now(),
        };

        playlist.create_entry(pool.clone()).unwrap();

        assert!(playlist.id_playlist > 0);
    }
}
